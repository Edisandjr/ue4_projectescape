// Edison Sandoval 2020

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/AudioComponent.h"
#include "Engine/TriggerVolume.h"
#include "OpenDoor.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PROJECTESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void OpenDoor(float DeltaTime);
	void CloseDoor(float DeltaTime);
	float TotalMassOfActors(int Index) const;
	void FindAudioComponent();
	void CheckForPressurePlate();

	bool OpenDoorSound = false;
	bool CloseDoorSound = true;

private: 

	float InitialYaw;
	float CurrentYaw;
	float InitialTargetYaw;

	
	float DoorLastOpened = 0.f;
	UPROPERTY(EditAnywhere) float DoorCloseDelay = .5f;
	UPROPERTY(EditAnywhere) int DoorOpenSpeed = 1;
	UPROPERTY(EditAnywhere) int DoorCloseSpeed = 1;
	UPROPERTY(EditAnywhere) float WeightToTrigger = 50;
	UPROPERTY(EditAnywhere) float OpenAngle;

	UPROPERTY(EditAnywhere) TArray<ATriggerVolume*> PressurePlate;
	//UPROPERTY(EditAnywhere) ATriggerVolume* PressurePlateClose;
	UPROPERTY(EditAnywhere) UAudioComponent* AudioComponent = nullptr;
	UPROPERTY(EditAnywhere) AActor* ActorThatOpens;
};
