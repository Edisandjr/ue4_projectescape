// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ProjectEscapeGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PROJECTESCAPE_API AProjectEscapeGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
