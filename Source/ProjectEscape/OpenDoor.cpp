// Edison Sandoval 2020
#include "OpenDoor.h"
#include "Components/AudioComponent.h"
#include "Components/PrimitiveComponent.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"
#include "GameFramework/PlayerController.h"
// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	InitialYaw = GetOwner()->GetActorRotation().Yaw;
	InitialTargetYaw = OpenAngle;
	CurrentYaw = InitialYaw;
	OpenAngle += InitialYaw;
	CheckForPressurePlate();
	FindAudioComponent();
}
void UOpenDoor::CheckForPressurePlate() 
{
	if (!PressurePlate.Num() == 0)
	{
		UE_LOG(LogTemp, Error, TEXT("%s has the OpenDoor Component, but no pressure plates aren't set!"), *GetOwner()->GetName());
	}
	ActorThatOpens = GetWorld()->GetFirstPlayerController()->GetPawn();
}
// Called every frame
void UOpenDoor::FindAudioComponent()
{
	AudioComponent = GetOwner()->FindComponentByClass<UAudioComponent>();
	if (!AudioComponent)
	{
		UE_LOG(LogTemp, Error, TEXT("Missing Audio Component!!"));
	}
}
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	for (int i = 0; i < PressurePlate.Num(); i++) {
		if (TotalMassOfActors(i) > WeightToTrigger)
		{
			OpenDoor(DeltaTime);
			DoorLastOpened = GetWorld()->GetTimeSeconds();
		}
		else
		{
			if (GetWorld()->GetTimeSeconds() - DoorLastOpened > DoorCloseDelay)
			{
				CloseDoor(DeltaTime);
			}
		}
	}
	//if (PressurePlateOpen && PressurePlateClose && PressurePlateClose->IsOverlappingActor(ActorThatOpens))
	//	CloseDoor(DeltaTime);
}
void UOpenDoor::OpenDoor(float DeltaTime) 
{
	OpenAngle = InitialTargetYaw;
	//UE_LOG(LogTemp, Warning, TEXT("OPEN CALLED"));
	CurrentYaw = FMath::FInterpTo(CurrentYaw, OpenAngle, DeltaTime, DoorOpenSpeed);
	FRotator DoorRotation = GetOwner()->GetActorRotation();
	DoorRotation.Yaw = CurrentYaw;
	GetOwner()->SetActorRotation(DoorRotation);

	CloseDoorSound = false;
	if (!AudioComponent) { return; }
	if (!OpenDoorSound) {
		if (DoorRotation.Yaw < 4.f) {
			OpenDoorSound = true;
			AudioComponent->Play();
		}
	}
}
void UOpenDoor::CloseDoor(float DeltaTime)
{
	//UE_LOG(LogTemp, Warning, TEXT("Close CALLED"));
	OpenAngle = InitialYaw;
	CurrentYaw = FMath::FInterpTo(CurrentYaw, OpenAngle, DeltaTime, DoorCloseSpeed);
	FRotator DoorRotation = GetOwner()->GetActorRotation();
	DoorRotation.Yaw = CurrentYaw;
	GetOwner()->SetActorRotation(DoorRotation);

	OpenDoorSound = false;
	if (!AudioComponent) { return; }
	if (!CloseDoorSound) {
		if (DoorRotation.Yaw < 4.f) {
			CloseDoorSound = true;
			AudioComponent->Play();
		}
	}
}
float UOpenDoor::TotalMassOfActors(int Index) const 
{
	float TotalMass = 0.f;
	//Find All Overlapping Actors
		if (PressurePlate[Index]) {
			TArray<AActor*> OverlappingActors;
			PressurePlate[Index]->GetOverlappingActors(OverlappingActors);
			for (int j = 0; j < OverlappingActors.Num(); j++) {
				TotalMass += OverlappingActors[j]->FindComponentByClass<UPrimitiveComponent>()->GetMass();
			}
		}

	UE_LOG(LogTemp, Warning, TEXT("Total Mass: %f"), TotalMass);
	return TotalMass;
}
/*

UE_LOG(LogTemp, Warning, TEXT("%s"), *GetOwner()->GetActorRotation().ToString());
UE_LOG(LogTemp, Warning, TEXT("Yaw is: %f"), GetOwner()->GetActorRotation().Yaw);


CurrentYaw = GetOwner()->GetActorRotation().Yaw;
FRotator OpenDoor{ 0.f, OpenAngle, 0.f };
OpenDoor.Yaw = FMath::FInterpTo(CurrentYaw, OpenAngle, DeltaTime, 1);
//FRotator OpenDoor = {0.f,90.f,0.5f};
GetOwner()->SetActorRotation(OpenDoor);
// ...
*/
